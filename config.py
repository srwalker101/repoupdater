# -*- coding: utf-8 -*-

# Username customisation, change these to string values containing your
# old username and new username
before_username = None
after_username = None

def change_username(remote_url):
    '''
    Mapping from the old url to the new url
    '''
    return remote_url.replace(before_username, after_username)
