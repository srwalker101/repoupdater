# Repo updater

When you change your username on [github](http://www.github.com/), your local repositories do not update.
I created this tool to go through all of your repositories and change your old username to your new username.

## Quick usage

The script is just run with one argument: the base directory from which the subdirectories are recursively altered.

**Important**: the repositories are only affected if the `-p/--perform` argument is given

## Requirements

The requirements are listed in [requirements.txt](requirements.txt).

## Installation

Install the python packages (in a virtualenv preferably):

    pip install -r requirements.txt

If you wish to contribute then install `py.test`:

    pip install -r requirements_devel.txt

Finally alter the `config.py` file and put your old username and new username in the file.
Optionally you can customise how the old url is converted to the new url by editing `change_username(remote_url)`.
Some tests will fail without doing this.

### Warning

The tests pass but I've never tried this! I haven't changed my username yet, so good luck! Let me know how it goes and I take no responsibility for any failures.

