import os
import updateurls as u
import pytest
import subprocess as sp
from dulwich.repo import Repo
from dulwich.config import ConfigFile

def setup_module(module):
    module.base_path = 'testing'

def test_is_git_dir():
    path = os.path.join(base_path, 'a')
    assert u.is_git_dir(path) == True

def test_list_test_directories():
    target = sorted(map(lambda p: os.path.join(base_path, p),
            ['a', 'b', '__pycache__', 'c']))

    results = sorted(list(u.recurse_directories(base_path)))
    assert results == target

def test_git_directories():
    target = [
            (os.path.join(base_path, 'a'), True),
            (os.path.join(base_path, 'b'), False),
            (os.path.join(base_path, 'c'), True),
            (os.path.join(base_path, '__pycache__'), False),
            ]

    results = sorted(u.git_directory_map(base_path))
    assert results == sorted(target)

def test_update_required():
    non_required = os.path.join(base_path, 'a')
    required = os.path.join(base_path, 'c')
    failure = os.path.join(base_path, 'b')

    assert u.is_update_required(non_required) == False
    assert u.is_update_required(required) == True

    with pytest.raises(OSError) as err:
        u.is_update_required(failure)



def test_context_changedir():
    chosen_path = os.path.join(base_path, 'b')
    with u.change_directory(chosen_path) as new_path:
        assert chosen_path in os.getcwd()

    assert chosen_path not in os.getcwd()

def test_get_remotes():
    assert list(u.get_remotes(os.path.join(base_path, 'a'))) == []
    assert list(u.get_remotes(os.path.join(base_path, 'c'))) == ['origin', ]

def test_get_remote_name():
    path = os.path.join(base_path, 'c')
    result = u.get_remote_url(path, 'origin')
    assert result == 'git://github.com/mindriot101/updategit-blank.git'

def test_update_remote_url(tmpdir):
    with u.change_directory(str(tmpdir)) as newpath:
        print "Located at {}".format(newpath)

        remote_url = 'git://github.com/mindriot101/updategit-blank.git'
        repo_dir = 'repo'
        cmd = ['git', 'clone', remote_url, repo_dir]
        sp.check_call(cmd)
        u.update_remotes(repo_dir)
        new_remote = u.get_remote_url(repo_dir, 'origin')
        assert new_remote == remote_url.replace('mindriot101', 'srwalker101')

def test_get_git_config():
    path = os.path.join(base_path, 'c')
    assert isinstance(u.get_git_config(path), ConfigFile)

def test_build_remotes():
    path = os.path.join(base_path, 'c')
    assert u.build_remote_mapping(path) == {'origin':  'git://github.com/mindriot101/updategit-blank.git'}
