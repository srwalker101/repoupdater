#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Usage:
    updateurls.py [options] <base_path>

Options:
    -h, --help          Show this help
    -p, --perform       Perform the changes
'''


from docopt import docopt
import os
import subprocess as sp
from dulwich.repo import Repo
from contextlib import contextmanager
import config

@contextmanager
def change_directory(dirname):
    old_cwd = os.getcwd()
    os.chdir(dirname)
    # Catch any exceptions that may occur and make sure we change directories
    # back
    try:
        yield os.getcwd()
    finally:
        os.chdir(old_cwd)

def recurse_directories(base_path):
    '''
    For a base path, recursively iterate through the contents, calling itself
    if a directory is found.

    Return a list of directories and whether the directory is a git repository
    '''
    for dirpath, dirnames, filenames in os.walk(base_path):
        for dirname in dirnames:
            if '.git' not in dirname and '.git' not in dirpath:
                yield os.path.join(dirpath, dirname)


def is_git_dir(path):
    return '.git' in os.listdir(path)

def get_git_config(path):
    repo = Repo(path)
    return repo.get_config()

def build_remote_mapping(path):
    '''
    Builds a dictionary mapping remotes to urls
    '''
    out = {}
    config = get_git_config(path)
    for key in get_git_config(path):
        try:
            if key[0] == 'remote':
                out[key[1]] = config[key]['url']
        except IndexError:
            continue

    return out


def get_remotes(path):
    return build_remote_mapping(path).keys()

def get_remote_url(path, remote_name):
    return build_remote_mapping(path)[remote_name]

def update_remotes(path, remote_changer=config.change_username):
    with change_directory(path):
        for remote in get_remotes('.'):
            current_remote_url = get_remote_url('.', remote)
            new_remote_url = remote_changer(current_remote_url)
            print "Updating {} => {}".format(current_remote_url, new_remote_url)
            cmd = ['git', 'remote', 'set-url', remote, new_remote_url]
            sp.check_call(cmd)


def is_update_required(dirname):
    '''
    Gets the git information and determines if the remote update is required
    '''
    with change_directory(dirname):
        if not '.git' in os.listdir('.'):
            raise OSError("Not a git directory: {}".format(
                os.path.realpath('.')))

        # Handle the remote info
        for remote in get_remotes('.'):
            url = get_remote_url('.', remote)
            if 'github.com' in url and config.before_username in url:
                return True

    return False


def git_directory_map(base_path):
    for dirname in recurse_directories(base_path):
        yield (dirname, is_git_dir(dirname))


def main(args):
    for directory in recurse_directories(args['<base_path>']):
        if is_git_dir(directory) and is_update_required(directory):
            print 'Updating {}'.format(directory)
            if args['--perform']:
                print "Updating remote"
                update_remotes(directory)
            else:
                print "Would update remote [--perform not given]"


if __name__ == '__main__':
    main(docopt(__doc__))
